/*
  Nombre: Alcancia.h
  Autor: Grupo IPOO
  Fecha Creacion: septiembre 03 de 2015
  Fecha Modificaci�n: enero 08 de 2019
  Versi�n : 5.0
  Email: alaponte@gmail.com
*/

/*
   Clase: Alcancia
   Responsabilidad:
    - Agregar monedas de diferentes denominaciones: $20, $50, $100, $200 y $500 unicamente.
    - Informar cu�ntas monedas hay de cada denominaci�n
    - Calcular el total de dinero ahorrado
    - Romper la alcanc�a despues de conocer el total ahorrado.
   Colaboraci�n: ninguna
*/

// Lo siguiente es una guarda. Es obligatorio ponerla en todo archivo.h
// La guarda evita que el archivo sea incluido mas de una vez en la implementacion.
// Para que funcione bien, la etiqueta (en este caso ALCANCIA_H)
// Debe ser �nica, o sea, no estar repetida en ningun otro archivo del proyecto.
//  - El compilador solo compila lo que hay entre las directivas
//    #ifndef ETIQUETA y #endif si la ETIQUETA no existe.
//  - La directiva #define ETIQUETA cree una nueva ETIQUETA

#include <string>
using namespace std;
#ifndef ALCANCIA_H
#define ALCANCIA_H

class Alcancia
{
    public:
        Alcancia();
        ~Alcancia();
        void agregarMoneda(int);
        int darMoneda(int);
        int darTotalDinero();
        bool estaRota();
        void romperAlcancia();
    protected:
    private:
        int numeroMonedas20;
        int numeroMonedas50;
        int numeroMonedas100;
        int numeroMonedas200;
        int numeroMonedas500;
        bool rota;
};

#endif // ALCANCIA_H
