/*
  Nombre: Menu.h
  Autor: Grupo IPOO
  Fecha Creacion: septiembre 03 de 2015
  Fecha Modificaci�n: septiembre 03 de 2015
  Versi�n : 1.0
  Email: alaponte@gmail.com
*/

/*
    Clase: Menu
    Responsabilidad:
    - Visualizar los diferentes servicios que ofrece el modelo, en este caso la alcanc�a
    - Permitir al usuario seleccionar las opciones de sercivios que ofrece el modelo.
    Colaboracion:  alcancia
*/

#include <iostream>
#include <alcancia.h>

using namespace std;

#ifndef MENU_H
#define MENU_H


class Menu
{
    public:
        Menu();
        void seleccionarOpcion();
        void visualizar();
        void agregarMonedas(int moneda);
        void romperAlcancia();
        void crearNuevaAlcancia();
        void mostrarEstadoAlcancia();

        virtual ~Menu();
    protected:
    private:
        int opcion, moneda;
        Alcancia *marranito;
};

#endif // MENU_H
