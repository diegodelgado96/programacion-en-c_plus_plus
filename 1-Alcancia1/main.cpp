/*
  Nombre: main.cpp
  Autor: Grupo IPOO
  Fecha Creacion: septiembre 03 de 2015
  Fecha Modificaci�n: septiembre 03 de 2015
  Versi�n : 1.0
  Email: alaponte@gmail.com
*/

#include <iostream>
#include <alcancia.h>
#include <menu.h>

using namespace std;

int main()
{
    Menu interfazAlcancia;
    interfazAlcancia.seleccionarOpcion();
    return 0;
}
