/*
  Nombre: Alcancia.cpp
  Autor: Grupo IPOO
  Fecha Creacion: septiembre 03 de 2015
  Fecha Modificaci�n: septiembre 03 de 2015
  Versi�n : 1.0
  Email: alaponte@gmail.com
*/

#include "Alcancia.h"


//Constructor
Alcancia::Alcancia()
{
    numeroMonedas20 = 0;
    numeroMonedas50 = 0;
    numeroMonedas100 = 0;
    numeroMonedas200 = 0;
    numeroMonedas500 = 0;
    rota = false;
}

Alcancia::~Alcancia()
{
    //dtor
}

//Este metodo me permite retornar el numero de monedas que tengo dependiendo su 
//denominacion; denominacion que es ingresada por parametro
int Alcancia::darMoneda(int moneda)
{
    switch(moneda)
    {
    case 20 :
        moneda = numeroMonedas20;
        break;

    case 50:
        moneda = numeroMonedas50;
        break;

    case 100:
        moneda = numeroMonedas100;
        break;

    case 200:
        moneda = numeroMonedas200;
        break;

    case 500:
        moneda = numeroMonedas500;
        break;
    }
    return moneda;
}

//Este metodo me permite adicionarle una moneda a su denominacion; 
//denominacion que es entrgada por parametro.
void Alcancia::agregarMoneda(int valor)
{
    switch(valor)
    {
    case 20 :
        numeroMonedas20++;
        break;

    case 50:
        numeroMonedas50++;
        break;

    case 100:
        numeroMonedas100++;
        break;

    case 200:
        numeroMonedas200++;
        break;

    case 500:
        numeroMonedas500++;
        break;
    }
}

//este metodo me retorna el total en pesos acumulados en mi alcancia
int Alcancia::darTotalDinero(){
    int total=numeroMonedas20*20+numeroMonedas50*50+numeroMonedas100*100+numeroMonedas200*200+numeroMonedas500*500;
    return total;
}

//Este metodo me retorna un booleano que me determina si la alcancia esta rota o no.
bool Alcancia::estaRota()
{
    return rota;
}


//Este metodo me permite reiniciar los valores de mi alcancia para haci poder crear una nueva.
void Alcancia::romperAlcancia()
{
        rota = true;
}
