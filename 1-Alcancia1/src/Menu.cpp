/*
  Nombre: Menu.cpp
  Autor: Grupo IPOO
  Fecha Creacion: septiembre 03 de 2015
  Fecha Modificaci�n: septiembre 03 de 2015
  Versi�n : 1.0
  Email: alaponte@gmail.com
*/

#include "Menu.h"

//Constructor
Menu::Menu()
{
    opcion = 0;
    marranito = new Alcancia;
    marranito->romperAlcancia();
}

Menu::~Menu()
{
    //dtor
}

//El metodo seleccionar opcion, es el que se encarga de mostrarme las opciones iniciales que puedo escoger.
//Este metodo me permite
//            - Agregar monedas a la alcancia -> "Siempre y caundo se encuentre creada una alcancia".
//            - Romper la alcancai -> "Siempre y caundo se encuentre creada una alcancia".
//            - Crear una alcancia -> "Siempre y caundo no se encuentre creada una alcancia".
//            - Estado de la alcancia -> "Siempre y caundo se encuentre creada una alcancia".
//            - Salir del programa
//En caso de dar una opcion invalida el programa me seguira pidiendo que ingrese una opcion valida.
//En caso de ser una opcion valida y no sea la opcion de salir se llamara al metodo Visualizar();
void Menu::seleccionarOpcion()
{
    do
    {
        cout << endl;
        cout << "        Alcancia   " << endl;
        cout << "1. - Agregar Moneda " << endl;
        cout << "2. - Romper Alcancia " << endl;
        cout << "3. - Crear nueva alcancia " << endl;
        cout << "4. - Estado Alcancia" << endl;
        cout << "5. - Salir " << endl;
        do{
            cout << "Introduzca Opci�n: ";
            cin >> opcion;
        }while (!((opcion>=1) && (opcion <=5)));
        visualizar();
    }while(opcion != 5);

}


//El metodo visualizar se encarga de capturar en SWITCH la opcion digitada, dependiendo lo que el usuario
//digite me redicionara a el metodo correspondiente.
void Menu::visualizar()
{
    switch(opcion){
        case 1:
            cout << "Digite la moneda a ingresar " << endl;
            cin >> moneda;
            agregarMonedas(moneda);
            break;
        case 2:
            romperAlcancia();
            break;
        case 3:
            crearNuevaAlcancia();
            break;
        case 4:
            mostrarEstadoAlcancia();
            break;
    }
}

//El metodo agregarMoneda recibe un valor por parametro "Cantidad"
//Este parametro es la denominación de la moneda que deseo agregar
//El metodo primero valida que la alcancia este creada o que no este rota
//          - si es asi entonces procedera capturar cantidad en un SWITCH y por medio del apuntador 
//            marranito->agregarMoneda(denominacion) llamara al metodo agregar moneda de la clase Alcancia;
//            dependiendo a la denominacion que le quiero agregar la moneda.
//            Recordemos que marranito es un objeto de tipo Alcancia.
//          - Si no es asi simplemente se le enviara un mensaje pidiendole que primero cree una alcancia
//            y se llamara al metodo seleccionarOpcion();
void Menu::agregarMonedas(int cantidad){
    cout << endl;
    if(!marranito->estaRota())
    {
        switch(cantidad)
        {
            case 20 :
                marranito->agregarMoneda(20);
                break;

            case 50:
                marranito->agregarMoneda(50);
                break;

            case 100:
                marranito->agregarMoneda(100);
                break;

            case 200:
                marranito->agregarMoneda(200);
                break;

            case 500:
                marranito->agregarMoneda(500);
                break;
        }
    }
    else
    {
        cout << "Debes crear una alcancia primero" << endl;
        seleccionarOpcion();
    }
}


//El metodo romperAlcancia() primero valida que la alcancia no este rota es decir que este creada
//              - De ser asi, se crea la variable totalDinero en la cual se va alojar lo que retorne el 
//                apuntador (marranito->darTotalDinero()) de la clase Alcancia, se invoca al apuntador 
//                marranito->romperAlcancia(), y se muestra por consola el total del dinero ahorrado.
//              - De no ser asi, simplemente se le enviara un mensaje pidiendole que primero cree una alcancia
//                y se llamara al metodo seleccionarOpcion();
void Menu::romperAlcancia(){
    if(!marranito->estaRota())
    {
        int totalDinero = marranito->darTotalDinero();
        marranito->romperAlcancia();
        cout << "El total del dinero ahorrado en la alcanc�a fue de ";
        cout << totalDinero << endl;
    }
    else
    {
        cout << "Debes crear una alcancia primero" << endl;
        seleccionarOpcion();
    }
}

// el metodo crearNuevaAlcancia() primero valida que la alcancia este rota es decir que no este creada
//              -De ser asi, creo una alcancoa nueva que se llamara marranito(Recordemos que marranito seria 
//               un objeto de tipo Alcancia) y mostraremos e estado inicial de la alcancia.
//              -De no ser asi, simplemente se le enviara un mensaje recordandole que alcancia ya fue creada antes
//               y se llamara al metodo seleccionarOpcion();
void Menu::crearNuevaAlcancia(){
    if(marranito->estaRota())
    {
        marranito = new Alcancia();
        mostrarEstadoAlcancia();
    }else{
        cout << "Ya existe una alcancia" << endl;
        seleccionarOpcion();
    }
}

//El metodo mostrarEstadoAlcancia() primero valida que la alcancia no este rota es decir que este creada
//          - De ser así, se mostara en pantalla el numeo de monedas por cada denominacion,
//            haciendo uso del puntero marranito->darMoneda(denominación) de la clase Alcancia
//          - De no ser asi, simplemente se le enviara un mensaje pidiendole que primero cree una alcancia
//            y se llamara al metodo seleccionarOpcion();
void Menu::mostrarEstadoAlcancia()
{
    if(!marranito->estaRota())
    {
        cout << "Monedas 20 = " << marranito->darMoneda(20)   << endl;
        cout << "Monedas 50 = " << marranito->darMoneda(50)   << endl;
        cout << "Monedas 100 = " << marranito->darMoneda(100) << endl;
        cout << "Monedas 200 = " << marranito->darMoneda(200) << endl;
        cout << "Monedas 500 = " << marranito->darMoneda(500) << endl;
    }
    else
    {
        cout << "Debes crear una alcancia primero" << endl;
        seleccionarOpcion();
    }
}
