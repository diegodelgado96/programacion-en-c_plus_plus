/*
  Asignatura: PROGRAMACI�N ORIENTADA A OBJETOS (IPOO) 750081M
  Archivo: amigo.h
  Fecha creaci�n: 07-septiembre-2011
  Fecha �ltima modificaci�n: 12-noviembre-2015
  Versi�n: 0.1
  Autor: Angel Garc�a Ba�os
  Grupo de Programaci�n
  ESCUELA DE INGENIER�A DE SISTEMAS Y COMPUTACI�N
  UniValle-2011
*/

#include "amigo.h"
using std::string;


//Se declara el primer constructor donde todos los datos detran por parametro
//Ya que los parametros tienen el mismo nombre de las variables globales; por consiguiente se
//usa la palabra reservada THIS; lo que hace que se refiera a las variables globales.
Amigo::Amigo(string nombre, int telefono, string ciudad)
{
  this->nombre = nombre;
  this->telefono = telefono;
  this->ciudad = ciudad;
}



Amigo::~Amigo()
{
    //dtor
}


//el metodo se_llama resibe como parametro cual_nombre de tipo string
//Este metodo retorna un booleano
//          -true: si el nombre es igual a cual_nombre
// true <==> Amigo::nombre==cual_nombre
bool Amigo::se_llama(string cual_nombre) const
{
  if(nombre == cual_nombre)
    return true;
  else
    return false;
}

//El metodo averiguar_telefono retorna el telefono del amigo
int Amigo::averiguar_telefono() const
{
  return telefono;
}

//Este metodo determina si el nombre de mi amigo esta de primero que el nombre de otro amigo
//otro es de tipo Amigo y entra por parametro
bool Amigo::operator<(const Amigo &otro) const
{
  return this->nombre < otro.nombre;
}

