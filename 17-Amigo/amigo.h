/*
  Asignatura: PROGRAMACI�N ORIENTADA A OBJETOS (IPOO) 750081M
  Archivo: amigo.h
  Fecha creaci�n: 07-septiembre-2011
  Fecha �ltima modificaci�n: 12-noviembre-2015
  Versi�n: 0.1
  Autor: Angel Garc�a Ba�os
  Grupo de Programaci�n
  ESCUELA DE INGENIER�A DE SISTEMAS Y COMPUTACI�N
  UniValle-2011
*/

#ifndef __CLASSAMIGO
#define __CLASSAMIGO

#include <string>

// Clase: Amigo
// Responsabilidad: mantiene el nombre, tel�fono y ciudad de un amigo. Se
//    puede averiguar cual es el tel�fono y si el nombre coincide
//    con uno dado (en caso de que no coincida, se puede preguntar
//    si est� antes o despu�s, seg�n el orden alfab�tico).
// Colaboraci�n: ninguna


class Amigo
{
  private:
    std::string nombre;
    int telefono;
    std::string ciudad;
  public:
    Amigo(std::string nombre = "", int telefono = 0, std::string ciudad = "Tulua");
    ~Amigo();
    bool se_llama(std::string cual_nombre) const; // true <==> Amigo::nombre==cual_nombre
    int averiguar_telefono() const;
    bool operator < (const Amigo &otro) const;  // Chequea si un nombre es anterior al del otro amigo, seg�n el orden alfab�tico
};

#endif

