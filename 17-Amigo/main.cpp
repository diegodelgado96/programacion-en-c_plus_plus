#include <iostream>
#include <Amigo.h>

using namespace std;

int main()
{
    Amigo *carlos = new Amigo( 1111111);
    Amigo *adriana = new Amigo("Adriana", 5789656, "Palmira");
    cout << "Telefono de Carlos es: " << carlos->averiguar_telefono() << endl;
    cout << "Telefono de Adriana es: " << adriana->averiguar_telefono() << endl;
//    cout << adriana->(&carlos)
    delete carlos;
    carlos = 0;
    delete adriana;
    adriana = 0;

    return 0;
}
