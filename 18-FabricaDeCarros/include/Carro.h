/*
  Asignatura: PROGRAMACI�N ORIENTADA A OBJETOS (IPOO) 750081M
  Archivo: carro.h
  Fecha creaci�n: 17-octubre-2001
  Fecha �ltima modificaci�n: 12-noviembre-2015
  Versi�n: 0.1
  Autor: Angel Garc�a Ba�os
  Grupo de Programaci�n
  ESCUELA DE INGENIER�A DE SISTEMAS Y COMPUTACI�N
  UniValle-2001
*/

#ifndef __CLASSCARRO
#define __CLASSCARRO

#include "luz.h"
#include <vector>
#include <iostream>

// Clase: Carro
// Responsabilidad: simula ser un carro con un motor de un cierto cilindraje,
//    y al que se le pueden a�adir luces "de lujo".
// Colaboraci�n: contiene luces


class Carro
{
  private:
    int cilindraje; // En cent�metros c�bicos
    std::vector<Luz > luces;
  public:
    Carro(int cilindraje);
    ~Carro();
    void anadir_luz(Luz luz);
    void imprimir(std::ostream &flujo);
    int obtenerCilindraje();
    std::vector<Luz > obtenerLuces();
};

#endif

