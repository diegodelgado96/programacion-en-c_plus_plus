


/*
  Asignatura: PROGRAMACI�N ORIENTADA A OBJETOS (IPOO) 750081M
  Archivo: fabricacarros.h
  Fecha creaci�n: 17-octubre-2001
  Fecha �ltima modificaci�n: 12-noviembre-2015
  Versi�n: 0.1
  Autor: Angel Garc�a Ba�os
  Grupo de Programaci�n
  ESCUELA DE INGENIER�A DE SISTEMAS Y COMPUTACI�N
  UniValle-2001
*/

#ifndef __CLASSFABRICACARROS
#define __CLASSFABRICACARROS

#include "luz.h"
#include "carro.h"
#include <vector>
#include <iostream>

// Clase: FabricaCarros
// Responsabilidad: Implementa el patr�n FACTOR�A, capaz de crear un carro
//    econ�mico o de lujo.
// Colaboraci�n: contiene luces


class FabricaCarros
{
  public:
    enum Tipo { ECONOMICO, LUJO };
  private:
    Tipo tipo;
  public:
    FabricaCarros(Tipo tipo);
    ~FabricaCarros();
    Carro * crear();
};

#endif

