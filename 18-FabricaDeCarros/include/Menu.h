
#include <iostream>
#include <fabricacarros.h>
#include <carro.h>
#include <luz.h>
#include <vector>

using namespace std;

#ifndef MENU_H
#define MENU_H


class Menu
{
    public:
        Menu();
        void seleccionarOpcion();
        void visualizar();
        virtual ~Menu();
    protected:
    private:
        int opcion;
        FabricaCarros *fabricaCarrosEconomicos;
        FabricaCarros *fabricaCarrosLujo;
        Carro *mi_carro;
        Carro *tu_carro;
        vector<Luz > luces;
};
#endif // MENU_H
