/*
  Asignatura: PROGRAMACI�N ORIENTADA A OBJETOS (IPOO) 750081M
  Archivo: luz.h
  Fecha creaci�n: 17-octubre-2001
  Fecha �ltima modificaci�n: 12-noviembre-2015
  Versi�n: 0.1
  Autor: Angel Garc�a Ba�os
  Grupo de Programaci�n
  ESCUELA DE INGENIER�A DE SISTEMAS Y COMPUTACI�N
  UniValle-2001
*/

#ifndef __CLASSLUZ
#define __CLASSLUZ

#include <iostream>
#include <string>

// Clase: Luz
// Responsabilidad: simula ser una luz de lujo de un carro, que se llama
//    de cierta manera y tiene una cierta potencia.
// Colaboraci�n: ninguna

class Luz
{
  private:
    std::string nombre;
    int potencia; // En watios
  public:
    Luz(std::string nombre = "", int potencia = 0);
    ~Luz();
    void imprimir(std::ostream &flujo);
    std::string obtenerNombre();
    int obtenerPotencia();
};

#endif

