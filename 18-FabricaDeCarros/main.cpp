#include <iostream>
#include <Menu.h>

/*
  Ejemplo 2: Mas manejo din�mico de memoria.
  - Esta vez, el new y el delete no est�n en la misma funci�n.
  - Se muestra el uso del PATR�N FACTORY (factor�a o f�brica),
    que es un objeto capaz de crear otros objetos.
*/

using namespace std;

int main()
{
    Menu interfaz;
    interfaz.visualizar();
    //cout << "Hello world!" << endl;
    return 0;
}
