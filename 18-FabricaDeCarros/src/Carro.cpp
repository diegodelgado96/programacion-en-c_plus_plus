/*
  Asignatura: PROGRAMACI�N ORIENTADA A OBJETOS (IPOO) 750081M
  Archivo: carro.cpp
  Fecha creaci�n: 17-octubre-2001
  Fecha �ltima modificaci�n: 12-noviembre-2015
  Versi�n: 0.1
  Autor: Angel Garc�a Ba�os
  Grupo de Programaci�n
  ESCUELA DE INGENIER�A DE SISTEMAS Y COMPUTACI�N
  UniValle-2001
*/

#include "carro.h"
using std::vector;
using std::ostream;
using std::endl;

Carro::Carro(int cilindraje)
{
  this->cilindraje = cilindraje;
  // inicialmente no tiene ninguna luz de lujo.
}

Carro::~Carro()
{
  // NO hay que haecr nada.
  // el vector de luces es una variable interna (local) de Carro, por lo
  // que se destruye sola al llegar aqu�.
  // A su vez, el vector destruye cada objeto que contenga.
}

void Carro::anadir_luz(Luz luz)
{
  luces.insert(luces.begin(), luz);
}

int Carro::obtenerCilindraje(){
    return cilindraje;
}

vector<Luz > Carro::obtenerLuces()
{
    return luces;
}

void Carro::imprimir(ostream &flujo)
{
  flujo << "Es de " << cilindraje << " cent�metros c�bicos" << endl;

  for(vector<Luz>::iterator cual = luces.begin(); cual != luces.end(); cual++)
  {
    cual->imprimir(flujo);
    flujo << endl;
  }
}
