/*
  Asignatura: PROGRAMACI�N ORIENTADA A OBJETOS (IPOO) 750081M
  Archivo: fabricacarros.cpp
  Fecha creaci�n: 17-octubre-2001
  Fecha �ltima modificaci�n: 12-noviembre-2015
  Versi�n: 0.1
  Autor: Angel Garc�a Ba�os
  Grupo de Programaci�n
  ESCUELA DE INGENIER�A DE SISTEMAS Y COMPUTACI�N
  UniValle-2001
*/

#include "fabricacarros.h"

FabricaCarros::FabricaCarros(Tipo tipo)
{
  this->tipo = tipo;
}


FabricaCarros::~FabricaCarros()
{
  // En los destructores de las f�bricas no hay que destruir el objeto construido
}


Carro * FabricaCarros::crear()
{
  Carro *carro;
  Luz luz1("exploradoras", 30);
  Luz luz2("direccionales", 10);
  Luz luz3("stop", 20);

  switch(tipo)
  {
    case ECONOMICO:
      carro = new Carro(1000);  // 1000 cent�metros c�bicos
      carro->anadir_luz(luz2);   // paso por valor (se hace copia de luz1)
    break;

    case LUJO:
      carro = new Carro(5000);  // 5000 cent�metros c�bicos
      carro->anadir_luz(luz1);   // paso por valor (se hace copia de luz1)
      carro->anadir_luz(luz2);   // paso por valor (se hace copia de luz2)
      carro->anadir_luz(luz3);   // paso por valor (se hace copia de luz3)
    break;
  }

  return carro; // Se retorna el carro, que ya est� listo.
}

