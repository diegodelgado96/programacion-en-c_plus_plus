#include "Menu.h"

Menu::Menu()
{
    opcion = 0;
    fabricaCarrosEconomicos = new FabricaCarros(FabricaCarros::ECONOMICO);
    fabricaCarrosLujo = new FabricaCarros(FabricaCarros::LUJO);
}

Menu::~Menu()
{
  delete fabricaCarrosEconomicos;
  fabricaCarrosEconomicos = 0;
  delete fabricaCarrosLujo;
  fabricaCarrosLujo = 0;
}

void Menu::seleccionarOpcion()
{
    cout << endl;
    cout << "1. Crear carro economico " << endl;
    cout << "2. Crear carro lujoso " << endl;
    cout << "3. Mostrar informaci�n de carro economico " << endl;
    cout << "4. Mostrar informaci�n de carro lujoso " << endl;
    cout << "5. Salir- " << endl;

    do{
        cout << "Introduzca Opci�n: ";
        cin >> opcion;
    }while (!((opcion>=1) && (opcion <=5)));
}

void Menu::visualizar()
{
    do{
        seleccionarOpcion();
        switch(opcion){
        case 1:
            cout << "Creando carro economico" << endl;
            mi_carro = fabricaCarrosEconomicos->crear();

            break;
        case 2:
            cout << "Creando carro lujoso " << endl;
            tu_carro = fabricaCarrosLujo->crear();
            break;
        case 3:
            cout << "El equipamiento de mi carro es:" << endl;
            luces = mi_carro->obtenerLuces();
            cout << "El cilindraje de mi carro es: " << mi_carro->obtenerCilindraje()<<endl;
            for(vector <Luz>::iterator cual = luces.begin(); cual != luces.end(); cual++)
            {
                cout << "Luz: " << cual->obtenerNombre() << endl;
                cout << "Potencia: " << cual->obtenerPotencia() << endl;
            }
            break;
        case 4:
            cout << "El equipamiento de tu carro es:" << endl;
            luces = tu_carro->obtenerLuces();
            cout << "El cilindraje de mi carro es: " << tu_carro->obtenerCilindraje()<<endl;
            for(vector <Luz>::iterator cual = luces.begin(); cual != luces.end(); cual++)
            {
                cout << "Luz: " << cual->obtenerNombre() << endl;
                cout << "Potencia: " << cual->obtenerPotencia() << endl;
            }
            break;
        case 5:
            delete mi_carro;// Aqu� se destruye el objeto mi_carro
            mi_carro = 0;// esto es muy conveniente
            delete tu_carro;// Aqu� se destruye el objeto tu_carro
            tu_carro = 0;// esto es muy conveniente
            break;
        }
    }while (opcion != 5);
}
