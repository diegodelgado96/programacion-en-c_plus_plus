/*
  Asignatura: PROGRAMACI�N ORIENTADA A OBJETOS (IPOO) 750081M
  Archivo: luz.cpp
  Fecha creaci�n: 17-octubre-2001
  Fecha �ltima modificaci�n: 12-noviembre-2015
  Versi�n: 0.1
  Autor: Angel Garc�a Ba�os
  Grupo de Programaci�n
  ESCUELA DE INGENIER�A DE SISTEMAS Y COMPUTACI�N
  UniValle-2001
*/

#include "luz.h"
using std::string;
using std::ostream;

Luz::Luz(std::string nombre, int potencia)
{
  this->nombre = nombre;
  this->potencia = potencia;
}

Luz::~Luz()
{
  // No hay que hacer nada
}

string Luz::obtenerNombre(){
    return nombre;
}

int Luz::obtenerPotencia(){
    return potencia;
}

void Luz::imprimir(ostream &flujo)
{
  flujo << nombre << " de " << potencia << " watios. ";
}
