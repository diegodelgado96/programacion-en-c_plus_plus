

#include <string>
#include "Fecha.h"
using namespace std;

#ifndef EMPLEADO_H
#define EMPLEADO_H

/*
   Clase:
   Responsabilidad:
   Colaboración:
*/

class Empleado
{
public:
    Empleado();
    Empleado(string dNombre, string dApellido, int dSexo, int dSalario, Fecha dFechaI, Fecha dFechaN);
    virtual ~Empleado();
    void cambiarSalario(int nuevoSalario); //cambia el salario no retorna nada
    int darSalario();
    int calcularEdad(); //retorna un int
    int calcularAntiguedad();
    int calcularPrestaciones();
    string darNombre();
    string darApellido();
    int darSexo();
    void subalternosACargo(int);
    int numeroSubalternos();
    int bonificacionSubalternos(double);
protected:
private:
    string nombre;
    string apellido;
    int sexo;
    int salario;
    Fecha fechaNacimiento;
    Fecha fechaIngreso;
    int subalternos;
};
#endif // EMPLEADO_H
