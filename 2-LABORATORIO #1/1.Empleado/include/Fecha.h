


#ifndef FECHA_H
#define FECHA_H

/*
   Clase: Fecha
   Responsabilidad: Simula una fecha donde puedo obtener el dia, mes y a�o, y calcular
                    la diferencia entre dos fechas dado en meses.
   Colaboraci�n: ninguna
*/
#include <ctime>
#include <iostream>

using namespace std;
class Fecha
{
public:
    Fecha();
    Fecha(int dDia, int dMes, int dAnio); //Constructor con 3 argumentos
    virtual ~Fecha();
    int darDia();
    int darMes();
    int darAnio();
    int darEnMeses(Fecha hoy);
protected:
private:
    int dia;
    int mes;
    int anio;
};
#endif // FECHA_H
