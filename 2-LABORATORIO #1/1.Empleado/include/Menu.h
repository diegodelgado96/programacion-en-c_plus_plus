#include <iostream>
#include "Empleado.h"
#include "Fecha.h"
#include <stdlib.h>
using namespace std;

#ifndef MENU_H
#define MENU_H

class Menu
{
    public:
        Menu();
        void seleccionarOpcion();
        void visualizar();
        void mostrarDatosPersonales();
        void mostrarCalculos();
        virtual ~Menu();
    protected:
    private:
        int opcion;
        Empleado e1;
        Fecha fn;
        Fecha fi;
        int edad, antiguedad, prestaciones, sex;
};

#endif // MENU_H
