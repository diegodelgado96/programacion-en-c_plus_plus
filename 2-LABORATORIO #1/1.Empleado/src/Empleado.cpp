
#include "Empleado.h"

Empleado::Empleado()
{

}
Empleado::Empleado(string dNombre, string dApellido, int dSexo, int dSalario, Fecha dFechaN, Fecha dFechaI)
{
  nombre = dNombre;
  apellido = dApellido;
  sexo = dSexo;
  salario = dSalario;
  fechaNacimiento = dFechaN;
  fechaIngreso = dFechaI;
}

Empleado::~Empleado()
{
    //dtor
}

void Empleado::cambiarSalario(int nuevoSalario)
{
    salario = nuevoSalario;
}

//En este caso es necesario ingresar la fecha actual para poder calcula la edad
int Empleado::calcularEdad()
{
    Fecha hoy(11,9,2015);
    int edad = fechaNacimiento.darEnMeses(hoy)/12;
    return edad;
}

int Empleado::calcularAntiguedad()
{
    Fecha hoy(11,9,2015);
    //a continuaci�n se invoca el m�todo darEnMeses(), es necesario realizarlo
    // a trav�s de un objeto de la clase fecha por que el m�todo pertenece a la clase
    //Fecha.
    int antiguedad = fechaIngreso.darEnMeses(hoy)/12;
    return antiguedad;
}

int Empleado::calcularPrestaciones()
{
    //a continuaci�n se invoca el m�todo calcularAntiguedad() de la clase Empleado, no es necesario
    //invocarlo a trav�s de un objeto por que estamos en la misma clase.
    int antiguedad = calcularAntiguedad();
    int prestaciones = (((salario * antiguedad)/12)+ ((salario*(100 / 6))*subalternos));
    return prestaciones;
}

string Empleado::darNombre()
{
    return nombre;
}

string Empleado::darApellido()
{
    return apellido;
}

int Empleado::darSexo()
{
    return sexo;
}

int Empleado::darSalario()
{
    return salario;
}
void Empleado::subalternosACargo(int numSubalternos)
{
    subalternos = numSubalternos;
}
int Empleado::numeroSubalternos()
{
    return subalternos;
}

int Empleado::bonificacionSubalternos(double bonif)
{
    bonif = ((salario*(100 / 6))*subalternos);
    return bonif;
}
