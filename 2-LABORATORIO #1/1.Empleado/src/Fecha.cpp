#include "Fecha.h"

Fecha::Fecha()
{
    /*time_t tiempoActual = time(NULL);
    struct tm* ptFActual = localtime(&tiempoActual);
    dia = ptFActual->tm_mday;
    mes = ptFActual->tm_mon + 1;
    anio = ptFActual->tm_year + 1900;*/
}

Fecha::~Fecha()
{
}

Fecha::Fecha(int dDia, int dMes, int dAnio)
{
    dia = dDia;
    mes = dMes;
    anio = dAnio;
}

int Fecha::darAnio()
{
    return anio;
}

int Fecha::darMes()
{
    return mes;
}

int Fecha::darDia()
{
    return dia;
}

int Fecha::darEnMeses(Fecha hoy)
{
    int diferencia = 0;
    int a = hoy.darAnio() - anio;
    int m = hoy.darMes() - mes;
    int d = hoy.darDia() - dia;
    diferencia = 12*a + m;

    if (d < 0)
        diferencia --; // -- es el operador decremento  <=> diferencia = diferencia -1
    return diferencia;
}
