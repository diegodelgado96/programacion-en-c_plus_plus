#include "Fecha.h"
#include "Menu.h"

Menu::Menu()
{
    opcion = 0;
    Fecha f1(4,9,1972); //fecha nacimiento
    Fecha f2(1,1,2004); //fecha ingreso
    fn = f1;
    fi = f2;
    Empleado e("albeiro", "aponte", 1, 2000000,fn,fi);
    e1 = e;
    edad = 0, prestaciones = e1.calcularPrestaciones(), antiguedad = 0;
    sex = e1.darSexo();
}

Menu::~Menu()
{
    //dtor
}

void Menu::seleccionarOpcion()
{
    cout << endl;
    cout << "1. - Modificar Salario" << endl;
    cout << "2. - Calcular Edad" << endl;
    cout << "3. - Calcular Antiguedad" << endl;
    cout << "4. - Calcular Prestaciones" << endl;
    cout << "5. - Numero de personas a cargo" << endl;
    cout << "6. - Mostrar sexo" << endl;
    cout << "7. - Salir" << endl;
    do{
        cout << "Introduzca Opci�n: ";
        cin >> opcion;
    }while (!((opcion>=1) && (opcion <=7)));
}

void Menu::visualizar()
{
    int nuevoSalario = 0;
    int numSubalternos = 0;

    do{
        mostrarDatosPersonales();
        mostrarCalculos();
        seleccionarOpcion();
        switch(opcion){
        case 1:
            cout << "Introduzca el nuevo salario" << endl;
            cin >> nuevoSalario;
            e1.cambiarSalario(nuevoSalario);
            break;
        case 2:
            edad = e1.calcularEdad();
            cout << "la edad es :" << e1.calcularEdad() << endl;
                system("pause");
            break;
        case 3:
            antiguedad = e1.calcularAntiguedad();
            cout << "la antiguedad es : " << e1.calcularAntiguedad() << endl;
                system("pause");
            break;
        case 4:
            prestaciones = e1.calcularPrestaciones();
            cout << "las prestasiones son : " << e1.calcularPrestaciones() << endl;
                system("pause");
            break;

        case 5:
            cout << "Introdusca el numero de personas a cargo" << endl ;
            cin >>  numSubalternos;
            e1.subalternosACargo(numSubalternos);
            break;

        case 6:
            sex = e1.darSexo();
            cout << "el sexo es: " << e1.darSexo() <<endl;
                system("pause");
            break;
        }
        system("cls");
    }while (opcion != 7);
}

void Menu::mostrarDatosPersonales()
{
        int bonif = 0;
    cout << "Nombre: " << e1.darNombre() << endl;
    cout << "Apellido: " << e1.darApellido() << endl;
    cout << "Sexo: " << e1.darSexo() << endl;
    cout << "Fecha Nacimiento: " <<fn.darDia()<<"/"<<fn.darMes()<<"/"<<fn.darAnio()<<endl;
    cout << "Fecha Antiguedad: " <<fi.darDia()<<"/"<<fi.darMes()<<"/"<<fi.darAnio()<<endl;
    cout << "Salario: " << e1.darSalario()<<endl;
    cout << "Personas a cargo: " << e1.numeroSubalternos()<<endl;
//    cout << "Bonificacion personas a cargo : " << e1.bonificacionSubalternos() << endl;
    cout << endl ;
}

void Menu::mostrarCalculos()
{
    if(edad > 0)
        cout << "Edad = " << edad << endl;
    if(antiguedad > 0)
        cout << "Antiguedad = " << antiguedad << endl;
    if(prestaciones > 0)
        cout << "Prestaciones = " << prestaciones << endl;
    if (sex >0)
        cout << "sexo = " << sex << endl;
}
