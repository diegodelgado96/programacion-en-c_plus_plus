#include <iostream>
#include "Triangulo.h"
#include "Punto.h"
#include <stdlib.h>
using namespace std;

/*
  Nombre: Menu.h
  Autor: grupo 4
  Fecha Creacion: Septiembre 3 de 2015
  Fecha Modificación: septiembre 23 2015
  Versión : 1.0
  Email: diegodelgado045@gmail.com
*/
#ifndef MENU_H
#define MENU_H

class Menu
{
    public:
        Menu();
        void seleccionarOpcion();
        void visualizar();
        void mostrarDatos();
        void mostrarCalculos();
        virtual ~Menu();
    protected:
    private:
        int opcion;
        Punto pto1, pto2, pto3;
        Triangulo t1;

        double perimetro, area, altura;
};

#endif // MENU_H
