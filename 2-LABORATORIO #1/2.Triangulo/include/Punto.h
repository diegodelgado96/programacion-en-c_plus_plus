/*
  Nombre: Punto.h
  Autor: grupo 4
  Fecha Creacion: Septiembre 3 de 2015
  Fecha Modificación: septiembre 23 2015
  Versión : 1.0
  Email: diegodelgado045@gmail.com
*/

#ifndef __CLASSPUNTO
#define __CLASSPUNTO
/*
   Clase: Punto
   Responsabilidad:
    - Simula ser un punto  del plano cartesiano,de coordenadas (x,y).
    - A traves de ella, podemos crear, cambiar y devolver puntos en el plano cartesiano.
   Colaboración: ninguna
*/


class Punto
{
  private:
     int x;
     int y;
  public:
    Punto();
    Punto(int dX, int dY);
	~Punto();
	int darX();
	int darY();
	void cambiarX(int x1);
	void cambiarY(int y1);
};
#endif
