/*
  Nombre: Triangulo.h
  Autor: grupo 4
  Fecha Creacion: Septiembre 3 de 2015
  Fecha Modificación: septiembre 23 2015
  Versión : 1.0
  Email: diegodelgado045@gmail.com
*/

#ifndef __CLASSTRIANGULO
#define __CLASSTRIANGULO
/*
   Clase: Triangulo
   Responsabilidad:  Deben completarlo
   Colaboración: Deben completarlo
*/

#include "Punto.h"
#include <cmath> //esta biblioteca sirve para utilizar los métodos matematicos por ej:
                 //sqrt(y), pow(x,p)

class Triangulo
{
  private:
     Punto punto1;
     Punto punto2;
     Punto punto3;
  public:
	Triangulo(Punto dPunto1, Punto dPunto2, Punto dPunto3);
	Triangulo();
	double calcularLado1();
	double calcularLado2();
	double calcularLado3();
	double calcularPerimetro();
	double calcularArea();
	double calcularAltura();
	Punto darPunto1();
	Punto darPunto2();
	Punto darPunto3();
	void cambiarPunto1(int, int);
	void cambiarPunto2(int, int);
	void cambiarPunto3(int, int);
	char isoceles();
	char escaleno();
	char equilatero();

};
#endif
