/*
  Nombre: main.cpp
  Autor: grupo 4
  Fecha Creacion: Septiembre 3 de 2015
  Fecha Modificación: septiembre 23 2015
  Versión : 1.0
  Email: diegodelgado045@gmail.com
*/
#include <iostream>
#include <cmath>
#include "Menu.h"
#include "Triangulo.h"
#include "Punto.h"

using namespace std;

int main()
{
    Menu menuTriangulo;
    menuTriangulo.visualizar();
    return 0;
}
