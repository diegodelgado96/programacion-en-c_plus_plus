/*
  Nombre: Menu.cpp
  Autor: grupo 4
  Fecha Creacion: Septiembre 3 de 2015
  Fecha Modificación: septiembre 23 2015
  Versión : 1.0
  Email: diegodelgado045@gmail.com
*/
#include <iostream>
#include "Menu.h"
using namespace std;
Menu::Menu()
{
    opcion = 0;
    Punto p1(0,0);
    Punto p2(0,4);
    Punto p3(4,0);
    pto1 = p1;
    pto2 = p2;
    pto3 = p3;
    Triangulo t(pto1,pto2,pto3);
    t1 = t;
    perimetro = 0, area =0, altura = 0;
}

Menu::~Menu()
{
    //dtor
}

void Menu::seleccionarOpcion()
{
    cout << endl;
    cout << "1. - Cambiar punto 1" << endl;
    cout << "2. - Cambiar punto 2" << endl;
    cout << "3. - Cambiar punto 3" << endl;
    cout << "4. - Dar Valor" << endl;
    cout << "5. - Isoceles" << endl;
    cout << "6. - Escaleno" << endl;
    cout << "7. - Equilatero" << endl;
    cout << "8. - salir" << endl;
    do{
        cout << "Introduzca Opción: ";
        cin >> opcion;
    }while (!((opcion>=1) && (opcion <=8)));
}

void Menu::visualizar()
{
    int x, y;
    do{
        mostrarDatos();
        mostrarCalculos();
        seleccionarOpcion();
        switch(opcion){
        case 1:
            cout << "Introduzca la coordenada x" << endl;
            cin >> x;
            cout << "Introduzca la coordenada y" << endl;
            cin >> y;
            t1.cambiarPunto1(x,y);
            break;
        case 2:
            cout << "Introduzca la coordenada x" << endl;
            cin >> x;
            cout << "Introduzca la coordenada y" << endl;
            cin >> y;
            t1.cambiarPunto2(x,y);
            break;
        case 3:
            cout << "Introduzca la coordenada x" << endl;
            cin >> x;
            cout << "Introduzca la coordenada y" << endl;
            cin >> y;
            t1.cambiarPunto3(x,y);
            break;
        case 4:
            cout << "el valor de lado 1 es: " <<t1.calcularLado1()<<endl;
                system("pause");
            cout << "el valor de lado 2 es: " <<t1.calcularLado2()<<endl;
                system("pause");
            cout << "el valor de lado 3 es: " <<t1.calcularLado3()<<endl;
                system("pause");
            cout << "el perimetro es: " <<t1.calcularPerimetro()<<endl;
                system("pause");
            cout << "el area es: " <<t1.calcularArea()<<endl;
                system("pause");
            cout << "la altura es : " << t1.calcularAltura()<<endl;
                system("pause");
            break;

        case 5:
            cout << " " << t1.isoceles() <<endl;
            system("pause");
        break;

        case 6:
            cout << " " << t1.escaleno() <<endl;
            system("pause");
        break;

        case 7:
            cout << " " << t1.equilatero() <<endl;
            system("pause");
        break;
                    }
        system("cls");
    }while (opcion != 8);
}

void Menu::mostrarDatos()
{
    cout << " P1(" << t1.darPunto1().darX() << "," << t1.darPunto1().darY()<<")" << endl;
    cout << " P2(" << t1.darPunto2().darX() << "," << t1.darPunto2().darY()<<")" << endl;
    cout << " P3(" << t1.darPunto3().darX() << "," << t1.darPunto3().darY()<<")" << endl;
    cout << endl ;
}

void Menu::mostrarCalculos()
{
    perimetro = t1.calcularPerimetro();
    area = t1.calcularArea();
    altura = t1.calcularAltura();
    if(perimetro > 0)
        cout << "Perimetro = " << perimetro << endl;
    if(area > 0)
        cout << "Area = " << area << endl;
    if(altura > 0)
        cout << "Altura = " << altura << endl;
}
