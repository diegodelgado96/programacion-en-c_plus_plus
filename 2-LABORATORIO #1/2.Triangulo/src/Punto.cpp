/*
  Nombre: Punto.cpp
  Autor: Grupo de IPOO
  Fecha Creacion: Septiembre 3 de 2012
  Fecha Modificación: Septiembre 3 de 2012
  Versión : 1.0
  Email: alaponte@gmail.com
*/

#include "Punto.h"

Punto::Punto()
{
    x=0;
    y=0;
}

Punto::Punto(int dX, int dY )
{
    x = dX;
    y = dY;
}

Punto::~Punto()
{
}

void Punto::cambiarX(int x1)
{
    x = x1;
}

void Punto::cambiarY(int y1)
{
    y = y1;
}

int Punto::darX()
{
    return x;
}

int Punto::darY()
{
    return y;
}
