/*
  Nombre: Triangulo.cpp
  Autor: grupo 4
  Fecha Creacion: Septiembre 3 de 2015
  Fecha Modificaci�n: septiembre 23 2015
  Versi�n : 1.0
  Email: diegodelgado045@gmail.com
*/

#include "Triangulo.h"
#include <iostream>
using namespace std;

Triangulo::Triangulo(Punto dPunto1,Punto dPunto2, Punto dPunto3)
{
    punto1 = dPunto1;
    punto2 = dPunto2;
    punto3 = dPunto3;
}

Triangulo::Triangulo()
{

}

//utilice la formula de la distancia entre dos puntos para el m�todo darLado1.
//es necesario utilizar las funciones pow(x,2), sqrt(y) de la biblioteca cmath

double Triangulo::calcularLado1()
{
    //calcula las restas
    double valorX = punto1.darX()- punto2.darX(); //completarlo
    double valorY = punto1.darY()- punto2.darY(); //completarlo

    //calcula la distancia
    double distancia = sqrt(pow(valorX, 2) + pow(valorY, 2)); //completarlo
    return distancia;
}

double Triangulo::calcularLado2()
{
    //calcula las restas
    double valorX = punto1.darX()- punto3.darX();
    double valorY = punto1.darY()- punto3.darY();

    //calcula la distancia
    double distancia = sqrt(pow(valorX, 2) + pow(valorY, 2));
    return distancia;
}

double Triangulo::calcularLado3()
{
    //calcula las restas
    double valorX = punto2.darX()- punto3.darX(); //completarlo
    double valorY = punto2.darY()- punto3.darY(); //completarlo

    //calcula la distancia
    double distancia = sqrt(pow(valorX, 2) + pow(valorY, 2)); //completarlo
    return distancia;
}

double Triangulo::calcularPerimetro()
{
    return calcularLado1()+calcularLado2()+calcularLado3();
}

//Para calcular el area se puede utilizar la siguiente ecuaci�n
//RaizCuadrada(s * (s - Lado1) * (s - Lado2) * (s - Lado3))
//Donde s = perimetro / 2

double Triangulo::calcularArea() //falta por implementar con base en la informacion anterior
{
    double s = calcularPerimetro()/2;
    double area = sqrt(
                       s*(s - calcularLado1())*(s - calcularLado2()* (s - calcularLado3())));
    return area;
}

//Tenga en cuenta que area = (base * altura)/2
//Es necesario despejar altura
double Triangulo::calcularAltura() // falta por implementar
{
    double base = calcularLado1();
    double altura = (2*calcularArea())/base;
    return altura;
}

Punto Triangulo::darPunto1()
{
    return punto1;
}

Punto Triangulo::darPunto2()
{
    return punto2;
}

Punto Triangulo::darPunto3()
{
    return punto3;
}

void Triangulo::cambiarPunto1(int x1, int y1)
{
    punto1.cambiarX(x1);
    punto1.cambiarY(y1);
}

void Triangulo::cambiarPunto2(int x1, int y1)
{
    punto2.cambiarX(x1);
    punto2.cambiarY(y1);
}

void Triangulo::cambiarPunto3(int x1, int y1)
{
    punto3.cambiarX(x1);
    punto3.cambiarY(y1);
}
char Triangulo::isoceles()
{
   if ((calcularLado1()==calcularLado2())||(calcularLado1()==calcularLado3())||calcularLado2()==calcularLado3())

       cout << "Es Isoceles" ;

   else

       cout << "No es Isoceles" ;

}

char Triangulo::escaleno()
{
    if ((calcularLado1()!=calcularLado2())&&(calcularLado1()!=calcularLado3())&&calcularLado2()!=calcularLado3())

        cout << "Es Escaleno" ;

    else

        cout << "No es Escaleno" ;
}

char Triangulo::equilatero()
{
    if ((calcularLado1()==calcularLado2())&&(calcularLado1()==calcularLado3())&&calcularLado2()==calcularLado3())

        cout << "Es Equilatero" ;

    else

        cout << "No es Equilatero" ;
}
