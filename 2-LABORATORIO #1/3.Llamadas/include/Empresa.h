/*
  Nombre: Empresa.h
  Autor: Grupo IPOO Numero 4
  Fecha Creacion: septiembre 11 de 2015
  Fecha Modificación: septiembre 23 de 2015
  Versión : 1.0
  Email: juanmagoraldo@hotmail.com
*/



#ifndef EMPRESA_H
#define EMPRESA_H
#include "LineaTelefonica.h"

class Empresa
{
    public:
        Empresa();
        LineaTelefonica darLinea1();
        LineaTelefonica darLinea2();
        LineaTelefonica darLinea3();
        void agregarLlamadaLocalLinea1(int minutos);
        void agregarLlamadaLocalLinea2(int minutos);
        void agregarLlamadaLocalLinea3(int minutos);
        void agregarLlamadaLargaDistanciaLinea1(int minutos);
        void agregarLlamadaLargaDistanciaLinea2(int minutos);
        void agregarLlamadaLargaDistanciaLinea3(int minutos);
        void agregarLlamadaCelularLinea1(int minutos);
        void agregarLlamadaCelularLinea2(int minutos);
        void agregarLlamadaCelularLinea3(int minutos);
        int darTotalNumeroLlamadas();
        int darTotalMinutos();
        double darTotalCostoLlamadas();
        double darCostoPromedioMinuto();
        void reiniciar();
        virtual ~Empresa();

    protected:
    private:
        LineaTelefonica linea1;
        LineaTelefonica linea2;
        LineaTelefonica linea3;
};

#endif // EMPRESA_H
