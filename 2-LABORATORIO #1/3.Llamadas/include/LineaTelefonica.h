/*
  Nombre: LineaTelefonica.h
  Autor: Grupo IPOO Numero 4
  Fecha Creacion: septiembre 11 de 2015
  Fecha Modificación: septiembre 23 de 2015
  Versión : 1.0
  Email: juanmagoraldo@hotmail.com
*/


#ifndef LINEATELEFONICA_H
#define LINEATELEFONICA_H


class LineaTelefonica
{
    public:
        LineaTelefonica();
        LineaTelefonica(int numeroLlamadas, int numeroMinutos, double costoLlamadas);
        ~LineaTelefonica();
        void reiniciar();
        double darCostoLlamadas();
        int darNumeroLlamadas();
        int darNumeroMinutos();
        void agregarLlamadaLocal(int minutos);
        void agregarLlamadaLargaDistancia(int minutos);
        void agregarLlamadaCelular(int minutos);
//        virtual ~LineaTelefonica();


    protected:
    private:
        int numeroLlamadas;
        int numeroMinutos;
        double costoLlamadas;
};

#endif // LINEATELEFONICA_H
