 /*
  Nombre: Menu.h
  Autor: Grupo IPOO Numero 4
  Fecha Creacion: septiembre 11 de 2015
  Fecha Modificación: septiembre 23 de 2015
  Versión : 1.0
  Email: juanmagoraldo@hotmail.com
*/


#include <iostream>
#include <stdlib.h>
#include "Empresa.h"
#include "LineaTelefonica.h"

using namespace std;

#ifndef MENU_H
#define MENU_H

class Menu
{
    public:
        Menu();
        void seleccionarOpcion();
        void visualizar();
        void registrarLlamada(int, int , int);
        virtual ~Menu();
    protected:
    private:
        int opcion;
        Empresa p1;


};

#endif // MENU_H
