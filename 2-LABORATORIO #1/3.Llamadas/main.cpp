/*
  Nombre: main.cpp
  Autor: Grupo IPOO Numero 4
  Fecha Creacion: septiembre 11 de 2015
  Fecha Modificación: septiembre 23 de 2015
  Versión : 1.0
  Email: juanmagoraldo@hotmail.com
*/
#include <iostream>

#include "LineaTelefonica.h"
#include "Empresa.h"
#include "Menu.h"
using namespace std;
//Inicia el menu con el nombre menuLlamadas clase Menu
int main()
{
    Menu menuLlamadas;
    menuLlamadas.visualizar();
    return 0;
}
