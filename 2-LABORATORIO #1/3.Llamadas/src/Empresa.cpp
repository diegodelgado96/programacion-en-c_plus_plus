/*
  Nombre: Empresa.cpp
  Autor: Grupo IPOO Numero 4
  Fecha Creacion: septiembre 11 de 2015
  Fecha Modificación: septiembre 23 de 2015
  Versión : 1.0
  Email: juanmagoraldo@hotmail.com
*/


#include "Empresa.h"

Empresa::Empresa()
{
    //ctor
}

LineaTelefonica Empresa::darLinea1()
{
    return linea1;
}

LineaTelefonica Empresa::darLinea2()
{
    return linea2;
}

LineaTelefonica Empresa::darLinea3()
{
    return linea3;
}
// Agregan una llamada por linea y tipo de llamada
void Empresa::agregarLlamadaLocalLinea1(int minutos)
{
    linea1.agregarLlamadaLocal(minutos);
}

void Empresa::agregarLlamadaLocalLinea2(int minutos)
{
    linea2.agregarLlamadaLocal(minutos);
}

void Empresa::agregarLlamadaLocalLinea3(int minutos)
{
    linea3.agregarLlamadaLocal(minutos);
}

void Empresa::agregarLlamadaLargaDistanciaLinea1(int minutos)
{
    linea1.agregarLlamadaLargaDistancia(minutos);
}

void Empresa::agregarLlamadaLargaDistanciaLinea2(int minutos)
{
    linea2.agregarLlamadaLargaDistancia(minutos);
}

void Empresa::agregarLlamadaLargaDistanciaLinea3(int minutos)
{
    linea3.agregarLlamadaLargaDistancia(minutos);
}

void Empresa::agregarLlamadaCelularLinea1(int minutos)
{
    linea1.agregarLlamadaCelular( minutos);
}

void Empresa::agregarLlamadaCelularLinea2(int minutos)
{
    linea2.agregarLlamadaCelular(minutos);
}

void Empresa::agregarLlamadaCelularLinea3(int minutos)
{
    linea3.agregarLlamadaCelular(minutos);
}

int Empresa::darTotalNumeroLlamadas() //Suma el numero de llamadas de todas las lineas
{
    int total= linea1.darNumeroLlamadas() + linea2.darNumeroLlamadas() + linea3.darNumeroLlamadas();
    return total;
}

int Empresa::darTotalMinutos()  //Suma la duracion de llamadas de todas las lineas
{
    int total= linea1.darNumeroMinutos() + linea2.darNumeroMinutos() + linea3.darNumeroMinutos();
    return total;
}

double Empresa::darTotalCostoLlamadas() //Suma el csoto de llamadas de todas las lineas
{
    double total= linea1.darCostoLlamadas()+linea2.darCostoLlamadas()+linea3.darCostoLlamadas();
    return total;
}

double Empresa::darCostoPromedioMinuto() // Dado el costo  total de llamads y la divide entre las 3  tipos de llamadas
{
    double promedio= darTotalCostoLlamadas()/3;
    return promedio;
}

void Empresa::reiniciar()
{

}


Empresa::~Empresa()
{
    //dtor
}
