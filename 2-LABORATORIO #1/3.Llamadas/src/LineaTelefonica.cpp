/*
  Nombre:  LineaTelefonica.cpp
  Autor: Grupo IPOO Numero 4
  Fecha Creacion: septiembre 11 de 2015
  Fecha Modificaci�n: septiembre 23 de 2015
  Versi�n : 1.0
  Email: juanmagoraldo@hotmail.com
*/


#include "LineaTelefonica.h"

LineaTelefonica::LineaTelefonica()
{
    numeroLlamadas = 0 ;
    numeroMinutos = 0 ;
    costoLlamadas = 0 ;//ctor
}

void LineaTelefonica::reiniciar()
{
    numeroLlamadas = 0 ;
    numeroMinutos = 0 ;
    costoLlamadas = 0 ;//ctor
}


int LineaTelefonica::darNumeroLlamadas()
  {
    return numeroLlamadas;
  }


int LineaTelefonica::darNumeroMinutos()
    {
    return numeroMinutos;
  }

double LineaTelefonica::darCostoLlamadas()
  {
    return costoLlamadas;
  }

void LineaTelefonica::agregarLlamadaLocal(int minutos)
{
    numeroLlamadas++; // Cada vez que invoca la funci�n, es por que se realiza una llamada
    numeroMinutos += minutos; // Se suman los minutos que llevaban mas los a registrar
    costoLlamadas += minutos * 50; // Dado los minutos registrados, multiplica por el costo de la llamada

}

void LineaTelefonica::agregarLlamadaLargaDistancia(int minutos)
{
    numeroLlamadas++; // Cada vez que invoca la funci�n, es por que se realiza una llamada
    numeroMinutos += minutos; // Se suman los minutos que llevaban mas los a registrar
    costoLlamadas += minutos * 250; // Dado los minutos registrados, multiplica por el costo de la llamada

}

void LineaTelefonica::agregarLlamadaCelular(int minutos)
{
    numeroLlamadas++; // Cada vez que invoca la funci�n, es por que se realiza una llamada
    numeroMinutos += minutos; // Se suman los minutos que llevaban mas los a registrar
    costoLlamadas += minutos * 450; // Dado los minutos registrados, multiplica por el costo de la llamada

}
LineaTelefonica::~LineaTelefonica()
{
    //dtor
}
