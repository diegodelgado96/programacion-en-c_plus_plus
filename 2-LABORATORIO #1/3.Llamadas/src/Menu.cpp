/*
  Nombre: Menu.cpp
  Autor: Grupo IPOO Numero 4
  Fecha Creacion: septiembre 11 de 2015
  Fecha Modificación: septiembre 23 de 2015
  Versión : 1.0
  Email: juanmagoraldo@hotmail.com
*/



#include "Menu.h"
#include <iostream>
using namespace std;
Menu::Menu()
{

    //ctor
}

void Menu::seleccionarOpcion()
{
    // Muestra en pantalla las 4 opciones a selecionar, dado un numéro realiza un caso.

    system ("cls");
    cout << endl;
    cout << " 1. - Registrar una llamada" << endl;
    cout << " 2. - Mostrar informacion detallada de cada linea" << endl;
    cout << " 3. - Consolidado Total" << endl;
    cout << " 4. - Salir" << endl;

    cout << endl;
    do{
        cout << " Introduzca Opcion: ";

        cin >> opcion;
    }while (!((opcion>=0) && (opcion <=4)));
}




void Menu::visualizar()
{


    do{


        seleccionarOpcion(); // Llama al metodo seleccionarOpcion para que el usuario ingrese un caso
        int linea;
        int tipo;
        int minutos;



        switch(opcion)
        {


        {

         case 1:
             /* En el caso 1 se registra una llamada dado una linea y un tipo de llamada, LargaDistancia etc...
             Dado la linea, tipo y duracion de llamada, llama a registrarLlamada(int linea, int tipo, int minutos)
             con los datos ingresados por el usuario */
             system ("cls");
             cout << endl;
            cout << " Introduzca la Linea a usar" << endl;
            cout << endl;
            cout << " 1. Linea 1" << endl;
            cout << " 2. Linea 2" << endl;
            cout << " 3. Linea 3" << endl;

            cin >> linea;
            system ("cls");
            cout << endl;
            cout << " Introduzca el Tipo" << endl;
            cout << endl;
            cout << " 1. Llamada Local" << endl;
            cout << " 2. Llamada Larga Distancia" << endl;
            cout << " 3. Llamada a Celular" << endl;

            cin >> tipo;
            system ("cls");
            cout << endl;
            cout << " Introduzca la duracion de la llamada" << endl;
            cout << endl;
            cin >> minutos;
            system ("cls");

            registrarLlamada(linea, tipo, minutos);

            break;
           }



        {

        case 2:
            // Muestra la informacion especifica de cada linea, numero de llamadas, duracion y costos.

            system ("cls");

           cout << endl;
           cout <<"      Linea 1:" << endl;
           cout << "      Numero de llamadas realizadas : " << p1.darLinea1().darNumeroLlamadas() << endl;
           cout << "      Duracion de las llamadas :  " << p1.darLinea1().darNumeroMinutos() << endl;
           cout << "      Costo de las llamadas : " << p1.darLinea1().darCostoLlamadas() << endl;
           cout << endl;
            system ("pause");
            system ("cls");

            cout << endl;
           cout <<"      Linea 2:" << endl;
           cout << "      Numero de llamadas realizadas : " << p1.darLinea2().darNumeroLlamadas() << endl;
           cout << "      Duracion de las llamadas :  " << p1.darLinea2().darNumeroMinutos() << endl;
           cout << "      Costo de las llamadas : " << p1.darLinea2().darCostoLlamadas() << endl;
           cout << endl;
            system ("pause");
            system ("cls");


            cout << endl;
           cout <<"      Linea 3:" << endl;
           cout << "      Numero de llamadas realizadas : " << p1.darLinea3().darNumeroLlamadas() << endl;
           cout << "      Duracion de las llamadas :  " << p1.darLinea3().darNumeroMinutos() << endl;
           cout << "      Costo de las llamadas : " << p1.darLinea3().darCostoLlamadas() << endl;
           cout << endl;
            system ("pause");
            system ("cls");


            break; }

        {

        case 3:
            //Consolidado total de las 3 lineas:
            system ("cls");
            cout << endl;
            cout << " Total de costos de las 3 lineas:  "<< p1.darTotalCostoLlamadas() << endl;
            cout << " Duracion total de las 3 lineas:  "<< p1.darTotalMinutos() << endl;
            cout << " Promedio total de las 3 lineas:  " << p1.darCostoPromedioMinuto () << endl;
            cout << endl;
            system (" pause");
            break;
        }

        {
            system ("cls");
        case 4:
            //Consolidado total de las 3 lineas:
            p1.reiniciar();
            break;
        }
system ("cls");


}
    }while (opcion!=4);
}


void Menu::registrarLlamada(int linea, int tipo, int minutos)
{
    /* Dado una linea (1,2,3), tipo (1,2,3) y duración de una llamada; el metodo analisa en que Metodo agregarLlamada tiene
    que usar */
     if         ((linea== 1) && (tipo == 1))
                {
                 p1.agregarLlamadaLocalLinea1(minutos);
                }
                else if ((linea== 1) && (tipo == 2))
                {
                 p1.agregarLlamadaLargaDistanciaLinea1(minutos);
                }
                else if ((linea== 1) && (tipo == 3))
                {
                 p1.agregarLlamadaCelularLinea1(minutos);
                }


                //Linea 2

                else if ((linea== 2) && (tipo == 1))
                {
                p1.agregarLlamadaLocalLinea2(minutos);
                }
                else if ((linea== 2) && (tipo == 2))

                {
                p1.agregarLlamadaLargaDistanciaLinea2(minutos);
                }

                else if ((linea== 2) && (tipo == 3))

                {
                p1.agregarLlamadaCelularLinea2(minutos);
                }

                //Linea 3

                else if ((linea== 3) && (tipo == 1))
                {
                p1.agregarLlamadaLocalLinea3(minutos);
                }

                else if ((linea== 3) && (tipo == 2))
                {
                p1.agregarLlamadaLargaDistanciaLinea3(minutos);
                }

                else if ((linea== 3) && (tipo == 3))

                {
                p1.agregarLlamadaCelularLinea3(minutos);
                }
}



Menu::~Menu()
{
    //dtor
}
