#include <iostream>
#ifndef DEPORTISTA_H
#define DEPORTISTA_H

using namespace std;

class Deportista
{
    public:
        Deportista();
        virtual ~Deportista();
        virtual void hacerDeporte();
    protected:
    private:
};

#endif // DEPORTISTA_H
