#include <Deportista.h>
#ifndef FUTBOLISTA_H
#define FUTBOLISTA_H


class Futbolista
{
    public:
        Futbolista();
        virtual ~Futbolista();
        void hacerDeporte();
    protected:
    private:
};

#endif // FUTBOLISTA_H
