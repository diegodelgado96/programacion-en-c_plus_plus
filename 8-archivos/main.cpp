#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <algorithm>

using namespace std;

Main()
{
    cin.ignore();
}

void crearGuardarArcchivos()
{
    //Crear y guardar un archivo en la computadors

    //Capturo la forma como el usuario quiere llamar el archivo en la variable nombre
    string nombre;
    cout << "Como quieres llamar a tu archivo?: ";
    cin >> nombre;
    cout << endl;

    //se crea un fichero de escritura, se crea un objeto ofstream que se comporta como cout.
    //Recibie como parametro el archivo que se va a modificar o escribir; en este caso es la variable
    //nombre que contiene el nombre que usuario ingreso.
    ofstream archivoEntrada(nombre);
    //creamos una variable mensaje de tipo string donde guardaremos el mensaje que queremos adicionarle
    //a nuestro archivo
    //Este mensaje sera recibido por consola
    string mensaje;
    cout << "Escribe lo que quieres guardar dentro de tu archivo:" << endl;

    //getline me gusrdara el texto en la variable mensaje
    getline(cin, mensaje);
    cout << endl;
    //ahora le asignamos lo guardado en la variable mensaje al ofstream(archivoEntrada)
    archivoEntrada << mensaje;
    //cerramos el archivo para que no pueda seguir siendo modificado
    archivoEntrada.close();
}


//leer un archivo que esta en el computador
void leerArchivo()
{
    //Capturamos el nombre del archivo que queremos leer en la variable nombre de tipo string
    string nombre;
    cout << "ingrese el nombre del archivo que desea abrir:" << endl;
    //usamos el getline para capturar e nombre con espacios
    getline(cin, nombre);

    //Para abrir un fichero para lectura, debe crear un objeto ifstream que se usar� como cin.
    //Recibie como parametro el archivo que se va a leer o abrir; en este caso es la variable
    //nombre que contiene el nombre que usuario ingreso.
    ifstream leer(nombre);

    //se crean dos variables de tipo string s y linea
    //  - s: aqui se guardara el texto completo para mostrarse en pantalla
    //  -linea: aqui se guardara una a una la lineas o remglones del texto y luego sera adiciona a la variable s
    string s, linea;

    //se crea un ciclo que recorra el archivo linea a linea y se detendra cuando la linea sea vacia.
    while(getline(leer, linea))
    {
        s += linea + "\n";
    }
    //imprimimos el archivo
    cout << s;
    //cerramos el archivo para que no se pueda volver a leer
    leer.close();
}


//se lee un archivo de la computadora y se guarda en un vector de string se ordena alfabeticamente, se imprime por
//pantalla y se guarda en un archivo distinto del leido.
void ordenarPalabras()
{
    vector <string> palabras;
    //capturamos el nombre del archivo en la variable
    //el nombre del archivo es ingresado por consola
    string archivo;
    cout << "digite el nombre del archivo:" << endl;
    //getline me gusrdara el texto en la variable mensaje
    getline(cin, archivo);

    //Para abrir un fichero para lectura, debe crear un objeto ifstream que se usar� como cin.
    //usamos ifstream por que solo queremos leer el archivo y no modificarlo
    ifstream archivoLectura(archivo);

    //si el archivo no existe solo se envia un mensaje
    if(!archivoLectura)
    {
        cout << "No encontre el archivo '" << archivo + "'"<< endl;
        system("pause");
    }
    //de lo contrario creamos una variable aux de tipo string
    //      - aux: guardara palabras que luego seran a�adidas al vector palabras
    //se iniciara un ciclo que guardara palabra a palabra en la variable aux, que luego sera adicionada
    //al vector palabras usando el metodo push_back
    //el metodo push_back de la clase vector sirve para agregarle valores al vector palabras.
    //usamos la funcion sort para ordenar los valores del vector de mayor a menor indicandoles que
    //empiece la comparacion desde el inicio hasta la final del vector(usando begin y end)
    //por ultimo empezamos a imprimir todos los datos del vector ordenado usando un ciclo for
    else
    {
        string aux;
        while(archivoLectura >> aux)
            palabras.push_back(aux);
        sort(palabras.begin(), palabras.end());
        for(int cual=0; cual<palabras.size(); cual++)
            cout << palabras[cual] << endl;
    }
    archivoLectura.close();
}

void eliminarArchivo()
{
    //capturamos el nombre del archivo, dado por es usuario en la variable nombre
    string nombre;
    cout << "Ingresa el nombre del archivo que deseas eliminar: "<<endl;
    //getline me gusrdara el texto en la variable mensaje
    getline(cin, nombre);

    //remove Borra el archivo cuyo nombre se especifica en nombre de archivo.
    //Esta es una operaci�n realizada directamente en un archivo identificado por su nombre de archivo;
    //No hay corrientes involucradas en la operaci�n.
    //remove es de tipo entero
    //int remove(const char * nombre);

    //Si el archivo se elimina con �xito, se devuelve un valor cero.
    //En caso de  no eliminarce, se devuelve un valor distinto de cero es decir no encontro el archivo.
    if(remove(nombre.c_str()) == 0)
    {
        cout << "Se elimino correctamente." << endl;
    }
    else
    {
        cout << "El archivo no existe." << endl;
    }
}


void renombrarArchivo()
{
    string nombre;
    cout << "Ingrese el nombre del archivo que quiere renombrar: " << endl;
    getline(cin, nombre);
    ifstream leer(nombre);

    if(leer.good())
    {
        string renombre;
        cout << "Como quieres que se llame tu archivo ahora?" << endl;
        getline(cin, renombre);
        int rename(nombre.c_str(), renombre.c_str());
    }
    else
    {

    }
}


int main()
{
    //cin.ignore()  limpia el buffer de entrada para que en dicho string se guarden espacios y saltos de linea
    eliminarArchivo();
    return 0;
}
