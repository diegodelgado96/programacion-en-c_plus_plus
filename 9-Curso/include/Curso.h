#include <iostream>
using namespace std;
#ifndef CURSO_H
#define CURSO_H
class Curso
{
    public:
        Curso();
        ~Curso();
        void cambiarNota(int numEstudiante, double nota);
        int cuantosEncimaPromedio();
        double darNota(int numEstudiante);
        double calcularPromedio();
        bool alMenosUnCinco();
        double calcularLaNotaQueMasAparece();
    private:
        double notas[12];
};

#endif // CURSO_H
