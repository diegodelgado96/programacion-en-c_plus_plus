#include <iostream>
#include <curso.h>

using namespace std;

#ifndef MENU_H
#define MENU_H


class Menu
{
    public:
        Menu();
        void seleccionarOpcion();
        void visualizar();
        void ventanaRegistrarNotas();
        virtual ~Menu();
    protected:
    private:
        int opcion;
        Curso c;

};

#endif // MENU_H
