#ifndef OPERACIONES_H
#define OPERACIONES_H


class Operaciones
{
    public:
        Operaciones();
        double suma();
        double resta();
        double multiplicacion();
        double division();

        virtual ~Operaciones();

    protected:

    private:
};

#endif // OPERACIONES_H
