#include "Menu.h"
#include <Operaciones.h>
#include <iostream>
#include <cstdlib>

using namespace std;

Menu::Menu()
{
    //ctor
}


int Menu::visualizar()
{
    int opcion;
    Operaciones op;

    do
    {
        cout << "\t     CALCULADORA DE FRACCIONES.\n" << endl;
        cout << "Diguite la operacion que desea hacer.\n" << endl;
        cout << "1.Suma." << endl;
        cout << "2.Resta." << endl;
        cout << "3.Multiplicaion." << endl;
        cout << "4.Division.\n" << endl;
        cout << "5.Salir.\n" << endl;
        cout << "Diguite la opcion." << endl;
        cin >> opcion;
        cout << endl;

        switch(opcion)
        {
        case 1:
            op.suma();
            break;

        case 2:
            op.resta();
            break;

        case 3:
            op.multiplicacion();
            break;

        case 4:
            op.division();
            break;

        }
        system("cls");
    }while(opcion > 5);
}

Menu::~Menu()
{
    //dtor
}
